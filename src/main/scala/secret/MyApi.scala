package secret

import java.net.URL

import caliban.CalibanError.ExecutionError
import caliban.GraphQL.graphQL
import caliban.RootResolver
import caliban.schema.{ArgBuilder, Schema}
import secret.Data.{ID, Player, Action}
import zio.{IO, UIO, Task}
import Service.dbService._

import scala.util.Try

object MyApi {

  // support for URL
  implicit val urlSchema: Schema[Any, URL] = Schema.stringSchema.contramap(_.toString)
  implicit val urlArgBuilder: ArgBuilder[URL] = ArgBuilder.string.flatMap(
    url => Try(new URL(url)).fold(_ => Left(ExecutionError(s"Invalid URL $url")), Right(_))
  )

  // API Definition
  case class FindIdArgs(id: ID)

  case class Queries(findPlayer: FindIdArgs => Task[Player],
                     listAllPlayerNames: UIO[List[PlayerView]],
                     findPlayerMostDeaths: Task[Player],
                     findActionsByPlayerId: FindIdArgs => Task[List[Action]]
                    )

  // resolvers
  val queries =
    Queries(
      args => findPlayer(args.id),
      listAllPlayerNames,
      findPlayerMostDeaths,
      args => findActionsByPlayerId(args.id)
    )

  // interpreter
  val api = graphQL(RootResolver(queries))

  val interpreter = api.interpreter

}