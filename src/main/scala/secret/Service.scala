package secret

import java.net.URL

import secret.Data.{Player, Role}
import secret.Data._
import caliban.CalibanError.ExecutionError
import zio.{IO, Task, UIO}

trait Service {
  def findPlayer(id: ID): Task[Player]
  def listAllPlayerNames: UIO[List[PlayerView]]
  def findPlayerMostDeaths: Task[Player]
  def findActionsByPlayerId(id: ID): Task[List[Action]]
}

case class PlayerView(id: ID, name: String)

object Service {
  val dbService: Service = new Service {
    override def findPlayer(id: ID): Task[Player] =
      playerList.find(_.id == id) match {
        case Some(player) => IO.succeed(player)
        case None => IO.fail(
          ExecutionError(
            msg = s"Player with id $id not found",
          )
      )
      }

    override def listAllPlayerNames: UIO[List[PlayerView]] = IO.succeed(
      playerList.map(player =>
        PlayerView(
          player.id,
          player.name,
        )
      )
    )

    override def findPlayerMostDeaths: Task[Player] =
      Option(playerList.maxBy(_.deaths)) match {
        case Some(player: Player) => IO.succeed(player)
        case None => IO.fail(
          ExecutionError(
            msg = s"No players found",
          )
        )
      }

    override def findActionsByPlayerId(id: ID): Task[List[Action]] =
      playerList.find(_.id == id) match {
        case Some(_) => IO.succeed(actionList.filter(_.playerId == id))
        case None => IO.fail(
          ExecutionError(
            msg = s"Player with id $id not found",
          )
        )
      }
  }
}
