package part2

import part2.Api3.MyQuery
import zio.Chunk
import zio.query.{DataSource, Request, ZQuery}
import part2.models.Data._

object Api4 {
  type MyQuery[+A] = ZQuery[Any, Nothing, A]

  case class QueryArgs(count: Int)
  case class Query(orders: QueryArgs => MyQuery[List[OrderView]])

  case class OrderView(id: OrderId, customer: MyQuery[Customer], products: List[ProductOrderView])
  case class ProductOrderView(id: ProductId, details: MyQuery[ProductDetailsView], quantity: Int)
  case class ProductDetailsView(name: String, description: String, reviews: MyQuery[List[Review]])

  def resolver(dBService: DBService): Query = {

    case class GetCustomer(id: CustomerId) extends Request[Nothing, Customer]
    val CustomerDataSource: DataSource[Any, GetCustomer] =
      DataSource.fromFunctionBatchedM("CustomerDataSource")(
        requests => dBService.getCustomers(requests.map(_.id).toList).map(Chunk.fromIterable)
      )

    def getCustomer(id: CustomerId): MyQuery[Customer] = ZQuery.fromRequest(GetCustomer(id))(CustomerDataSource)

    case class GetProduct(id: ProductId) extends Request[Nothing, Product]
    val ProductDataSource: DataSource[Any, GetProduct] =
      DataSource.fromFunctionBatchedM("ProductDataSource")(
      requests => dBService.getProducts(requests.map(_.id).toList).map(Chunk.fromIterable)
      )

    def getProduct(id: ProductId): MyQuery[Product] = ZQuery.fromRequest(GetProduct(id))(ProductDataSource)

    case class GetReviews(ids: List[ReviewId]) extends Request[Nothing, List[Review]]
    val ReviewDataSource: DataSource[Any, GetReviews] =
      DataSource.fromFunctionM("ReviewDataSource")(req => dBService.getReviews(req.ids))
    def getReviews(ids: List[ReviewId]): MyQuery[List[Review]] = ZQuery.fromRequest(GetReviews(ids))(ReviewDataSource)

    def getOrders(count: Int): MyQuery[List[OrderView]] =
      ZQuery
      .fromEffect(dBService.getLastOrders(count))
      .map(_.map(order => OrderView(order.id, getCustomer(order.customerId), getProducts(order.products))))

      def getProducts(products: List[(ProductId, Int)]): List[ProductOrderView] =
        products.map {
          case (productId, quantity) =>
            ProductOrderView(
              productId,
              getProduct(productId).map(p => ProductDetailsView(p.name, p.description, getReviews(p.reviews))),
              quantity
            )
        }

  Query(args => getOrders(args.count))

  }
}
