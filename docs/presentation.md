---
%title: GraphQL and Caliban demo
%author: Yon-lu
%date: 2020-12-22

-> # Introduction to GraphQL

---

# Agenda

- GraphQL
<br>
    - Small detour to look at REST
<br>

- Caliban: a functional GraphQL library
<br>

- Some live coding
<br>

- GraphQL demo

---

# What is GraphQL?

- GraphQL is a query language for APIs
<br>

- Server exposes a typed schema
<br>

- Client asks for what they want
<br>
    - Queries (typically `HTTP GET`)
    <br>
    - Mutations (typically `HTTP POST`)
    <br>
    - Subscriptions (typically websockets)
    <br>
    - However, the GraphQL specification does not actually pose any restrictions on the transport layer used.

---

# The GraphQL sales pitch

Building APIs that: 

<br>
- are agnostic to the data source or programming language used
<br>
    - common misconception that GraphQL is a database language like SQL
<br>

- have a strongly-typed system
<br>

- reduce data payloads to clients and network bandwidth
<br>

Let's take a quick look at a short example of some features we can highlight
https://graphql.org/

---

# Prelude

Before going deeper into GraphQL, let's take a detour and look at REST

> REST: REpresentational State Transfer

---

# A REST example

```
// GET /orders/1 
 
{
  "customerId": 120,
  {
    "products": [
      {
        "productId": 298743,
        "price": {
          "total": 23,
          "tax": 6,
          "currency": EUR
        }
      },
      {
        "productId": 287543,
        "price": {
          "total": 14,
          "tax": 3,
          "currency": EUR
        }
      }
    ]
  }
  // ... more fields here
}

```
<br>
- Note: Some REST APIs would have the `products` as a separate resource

 
---

# Issues with REST

- The type, shape and how you fetch is tightly coupled to the resource
<br>
    - Server defines the schema and the client has to work with this inflexible format
<br>

- Over-fetching: 
<br>
    - Even if you do not require certain fields in a resource, they will be transmitted
    <br>
    - If the client does not even use the data, there's a waste in computer power and network bandwidth

<br>
- Also prone to under-fetching, which would then require multiple round trips (eg. to another endpoint) to fetch all the data required

---

# An alternative GraphQL schema

Fields are added only when the client requires.
eg. on the frontend, they know which fields are needed, and their query will only "project" these fields

```
query FindOrderById($input : Int!) {
    findOrderById(id: $input) {
        id
        productsList {
            product {
                productId
                price {
                    total
                    tax
                    currency
                }
            }
        }
    }
}
```

<br>

# JSON result:

```
{
   "data": {
        "id" : 1,
        "productsList": [
            {
                "product": {
                    "productId": "100",
                    "price": {
                        "total" : 50.0,
                        "tax" : 5.0,
                        "currency" : "USD"
                    }
                }
            }
        ]
    }
}

```

---

# GraphQL Benefits

- Client decides what data it requires. This makes working with the API more predictable

- Only one endpoint for your service

- Ability to introspect the self-documenting schema (shown in demo later)

---

# REST in peace?

*Is this the end of REST?*

Not really. While GraphQL is an alternative to REST, its not a definitive replacement.

The GraphQL docs also explain that GraphQL and REST can coexist, and you can abstract your REST APIs behind a GraphQL server,
by masking your REST endpoint into a GraphQL endpoint using root resolvers.

---

# Getting with GraphQL basics

The GraphQL docs are pretty good at giving a more detailed runthrough of all the specifications that you can use.

I'll only be going through the basics to get starting.

---





---

# Caliban: Functional GraphQL Library

- Minimal boilerplate

- Purely functional
    - Strongly typed
    - Explicit errors
    - ZIO to manage functional effects
    
---

# Caliban Code Generation

Developers working with GraphQL on the client side might write queries with external tools such as Insomnia, then copy pasting the code into their projects.

Although these tools are good for testing out the API, it can be error prone especially if the schema changes.

Caliban-client has a useful tool to generate boilerplate code from an existing GraphQL schema.

---

# Steps to generate boilerplate Scala code

1. Obtain the GraphQL SDL (Schema Definition Language)
2. Run a sbt command to generate boilerplate code 
(eg. `calibanGenClient <schemaPath> <outputPath> <?scalafmtPath>`)
3. Write GraphQL queries in plain Scala by calling the generated code and combining fields.


---


# Demo time


```

                                                                                                                     
 _____ _                  _        ___      _                   _                                                     
|_   _| |_ ___ ___ ___   |_|___   |_  |    |_|_____ ___ ___ ___| |_ ___ ___    ___ _____ ___ ___ ___    _ _ ___       
  | | |   | -_|  _| -_|  | |_ -|   _| |_   | |     | . | . |_ -|  _| . |  _|  | .'|     | . |   | . |  | | |_ -|_ _ _ 
  |_| |_|_|___|_| |___|  |_|___|  |_____|  |_|_|_|_|  _|___|___|_| |___|_|    |__,|_|_|_|___|_|_|_  |  |___|___|_|_|_|
                                                   |_|                                          |___|                 

```


---

# Let's get our hands dirty

1. SSH tunnel into gpu `ssh -L 8088:localhost:8088 gpus`
2. Boot up Insomnia or your favourite API client. Alternatively, you can also enter `http://localhost:8088/graphiql` in your browser.
3. Select the `GraphQL Demo` workspace, or just create a new workspace
4. Enter the endpoint `http://localhost:8088/api/graphql/` for your request

Since you've tunnelled to the gpu, the GraphQL service is hosted at `localhost:8088/api/graphql/` on the remote machine.

---

# Writing our first query



---

# Credits

- This tutorial was heavily inspired by ghostdogpr, creator of Caliban.
- For next steps on how to proceed from where this presentation left off, you can look at [Part 2](https://medium.com/@ghostdogpr/graphql-in-scala-with-caliban-part-2-c7762110c0f9) of his tutorial, 
where he introduces how to use `ZQuery` to optimise database calls with GraphQL.

